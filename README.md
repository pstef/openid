# An OpenID connector for PHP

[![Latest Stable Version](https://poser.pugx.org/pstef/openid/v/stable)](https://packagist.org/packages/pstef/openid)
[![License](https://poser.pugx.org/pstef/openid/license)](https://packagist.org/packages/pstef/openid)
[![Total Downloads](https://poser.pugx.org/pstef/openid/downloads)](https://packagist.org/packages/pstef/openid)

This library provides client-side OpenId Connect flow using PHP.
 
It's meant for use in a Symfony (>=4.1) application, to authenticate users against an OpenId provider like:
 
 - [Google](https://developers.google.com/identity/protocols/OpenIdConnect)
 - [Microsoft Azure AD](https://docs.microsoft.com/en/azure/active-directory/develop/v1-protocols-openid-connect-code) 
 - [PayPal](https://developer.paypal.com/docs/integration/direct/identity/#oauthopenidconnect)
 - [Yahoo](https://developer.yahoo.com/oauth2/guide/openid_connect/)

---

## Requirements

 - PHP >= 7.1.3
 - cURL extension
 - JSON extension


## Installation

    composer install pstef/openid



## Usage

Please have a look at the [Sample Symfony project](https://gitlab.com/pstef/symfony-openid-sample) for usage instructions.


## API

@todo


## See also

This code is based on the OpenId specs available [here](https://openid.net/developers/specs/), and in particular the OpenId 1.0  discovery process documented here:

[openid.net/specs/openid-connect-discovery-1_0.html](https://openid.net/specs/openid-connect-discovery-1_0.html)


