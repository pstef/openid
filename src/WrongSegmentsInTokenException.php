<?php

namespace Pstef\OpenId;


/**
 * This exception should be thrown when a JWT token is encountered, which has a number of segments which isn't 3 (three):
 * header, payload and signature.
 */
class WrongSegmentsInTokenException extends \Exception
{

}