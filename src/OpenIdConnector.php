<?php

namespace Pstef\OpenId;

use Exception;
use Firebase\JWT\JWT;
use Symfony\Component\HttpFoundation\Request;

/**
 * Basic Open Id connector class.
 * Really, just a bit more than a container for some variables.
 *
 * Copyright (C) 2018  Paolo Stefan <https://gitlab.com/pstef>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
class OpenIdConnector
{
  /**
   * The discovery endpoint should return a JSON object containing *at least* these keys.
   * @see https://openid.net/specs/openid-connect-discovery-1_0.html#ProviderMetadata
   */
  const REQUIRED_DISCOVERY_KEYS = [
    'issuer',
    'authorization_endpoint',
    'token_endpoint',
    'jwks_uri',
    'response_types_supported',
    'subject_types_supported',
    'id_token_signing_alg_values_supported'
  ];

  /**
   * Code to use in the call to the token endpoint.
   *
   * @see getTokenEndpointParameters
   * @var string
   */
  private $authorizationCode;

  /**
   * Application ID aka Client ID
   * @var string
   */
  private $clientId;

  /**
   * Application secret/password aka Client secret
   * @var string
   */
  private $clientSecret;

  /**
   * JSON-decoded response of the discovery endpoint.
   * @see the specs https://openid.net/specs/openid-connect-discovery-1_0.html#ProviderMetadata
   * @var array
   */
  private $discoveryData;

  /**
   * Discovery endpoint URL.
   * For MS Azure AD, it is https://login.microsoftonline.com/{tenant}/.well-known/openid-configuration .
   *
   * @var string
   */
  private $discoveryEndpoint;

  /**
   * True if the provider discovery has been performed succesfully.
   * @var bool
   */
  private $discoveryPerformed = false;

  /**
   * Additional parameters to pass to the authorization endpoint, e.g. ['domain_hint' => 'example.com']
   * @var array
   */
  private $extraParams;

  /**
   * Holds the content of the provider's JSON Web Keys Set document.
   * @see the specs https://tools.ietf.org/id/draft-ietf-jose-json-web-key-41.html
   * @var array
   */
  private $jwks;

  /**
   * Nonce (random string) to pass to the authorization endpoint.
   * The JWT token will hold the same value and, if it doesn't, it should be rejected.
   * This behaviour can be used to mitigate CSRF attacks.
   *
   * @var string
   */
  private $nonce;

  /**
   * _Optional_: the OpenId authorization endpoint will redirect to this URI.
   * If not set, the endpoint will decide which URI to call.
   *
   * @var string
   */
  private $redirectUri;

  /**
   * State string (random hash) to pass to the authorization endpoint.
   * The endpoint should redirect to $redirect_uri sending back the same state value.
   * This behaviour can be used to mitigate CSRF attacks.
   *
   * @var string
   */
  private $state;

  /**
   * Name of the session variable holding the state value
   * @var string
   */
  private $sessionStateKey = 'openid_state';

  /**
   * Name of the session variable holding the nonce value
   * @var string
   */
  private $sessionNonceKey = 'openid_nonce';

  /**
   * OpenIdConnector constructor. In Symfony, constructor parameters can be specified using a service.
   *
   * @param string $discoveryEndpoint
   * @param string $clientId
   * @param string|null $clientSecret
   * @param string|null $redirectUri
   * @param array|null $extraParams
   * @throws Exception
   */
  public function __construct(
    string $discoveryEndpoint,
    string $clientId,
    string $clientSecret = NULL,
    string $redirectUri = NULL,
    array $extraParams = NULL) {

    $this->discoveryEndpoint = $discoveryEndpoint;
    $this->clientId = $clientId;
    $this->clientSecret = $clientSecret;
    $this->redirectUri = $redirectUri;
    $this->extraParams = $extraParams;

    // By default, nonce and state are initialized as random strings
    $this->nonce = bin2hex(random_bytes(16));
    $this->state = bin2hex(random_bytes(8));
  }

  /**
   * @return mixed
   */
  public function getAuthorizationCode() {
    return $this->authorizationCode;
  }

  /**
   * @param mixed $authorizationCode
   * @return OpenIdConnector
   */
  public function setAuthorizationCode($authorizationCode) {
    $this->authorizationCode = $authorizationCode;
    return $this;
  }

  /**
   * @return string
   */
  public function getNonce() {
    return $this->nonce;
  }

  /**
   * @param string $nonce
   * @return OpenIdConnector
   */
  public function setNonce($nonce) {
    $this->nonce = $nonce;
    return $this;
  }

  /**
   * @return string
   */
  public function getState() {
    return $this->state;
  }

  /**
   * @param string $state
   * @return OpenIdConnector
   */
  public function setState($state) {
    $this->state = $state;
    return $this;
  }

  /**
   * @return array
   */
  public function getJWKS() {
    return $this->jwks;
  }

  /**
   * Returns the discovery endpoint URL.
   * @return string
   */
  public function getDiscoveryEndpoint(): string {
    return $this->discoveryEndpoint;
  }

  /**
   * Returns the End session (logout) endpoint URL. Calls discover() if $this->discoveryData is empty.
   *
   * @return string
   * @throws Exception if $discoveryData is empty and discovery does not succeed.
   */
  public function getEndSessionEndpoint() {
    if (empty($this->discoveryData)) {
      $this->discover();
    }

    return $this->discoveryData['end_session_endpoint'];
  }

  /**
   * Returns the Authorization (login) endpoint URL, including the proper parameters in the query string.
   *
   * @return string
   * @throws Exception if $discoveryData is empty and discovery does not succeed.
   */
  public function getAuthorizationEndpoint() {
    if (empty($this->discoveryData)) {
      $this->discover();
    }

    return $this->discoveryData['authorization_endpoint'] . '?' .
      http_build_query($this->getAuthorizationEndpointParameters());
  }

  /**
   * Returns the parameters to send to the Authorization (login) URL.
   * If the provider allows mixed response types, this method requests *code* and *id_token* response types at once.
   *
   * @return array
   * @throws Exception
   */
  public function getAuthorizationEndpointParameters() {

    $args = [
      'scope' => 'openid',
      'response_type' => $this->getSupportedResponseTypeWith(['code', 'id_token']),
      'client_id' => $this->clientId,
      'nonce' => $this->nonce,
      'state' => $this->state,
      'response_mode' => 'form_post',
    ];

    // Optional args
    if (!empty($this->redirectUri)) {
      $args['redirect_uri'] = $this->redirectUri;
    }

    // Additional, custom args
    if (!empty($this->extraParams)) {
      $args = array_merge($args, $this->extraParams);
    }

    return $args;
  }

  /**
   * Returns the Token endpoint URL.
   *
   * @return string
   * @throws Exception if $discovery_data is unset.
   */
  public function getTokenEndpoint() {
    if (empty($this->discoveryData)) {
      $this->discover();
    }

    return $this->discoveryData['token_endpoint'];
  }

  /**
   * Returns the parameters to send to the token endpoint.
   * Redirect URI and extra parameters (if any) are appended.
   *
   * At the moment I just followed the docs on the microsoft website:
   *
   * https://developer.microsoft.com/en-us/graph/docs/concepts/auth_v2_user#3-get-a-token
   *
   * @return array
   */
  public function getTokenEndpointParameters() {
    $args = [
      'client_id' => $this->clientId,
      'code' => $this->authorizationCode,
      'grant_type' => 'authorization_code',
      'client_secret' => $this->clientSecret,
    ];

    // Optional args
    if (!empty($this->redirectUri)) {
      $args['redirect_uri'] = $this->redirectUri;
    }

    // Additional, custom args
    if (!empty($this->extraParams)) {
      $args = array_merge($args, $this->extraParams);
    }

    return $args;
  }

  /**
   * Manually sets the discovery data array (usually it's populated by the discover() method).
   * Should be used only for testing purposes.
   *
   * @param array $discoveryData
   * @return OpenIdConnector
   */
  public function setDiscoveryData(array $discoveryData): OpenIdConnector {
    $this->discoveryData = $discoveryData;
    return $this;
  }

  /**
   * Adds or updates an extra parameter to pass to the authorization URL.
   *
   * @param string $paramName The name of the parameter to set.
   * @param mixed $value The parameter's value.
   * @return array|null The current value of $this->extraParams.
   */
  public function setExtraParameter(string $paramName, $value) {
    $this->extraParams[$paramName] = $value;
    return $this->extraParams;
  }

  /**
   * @return string
   */
  public function getSessionStateKey(): string {
    return $this->sessionStateKey;
  }

  /**
   * @param string $sessionStateKey
   * @return OpenIdConnector
   */
  public function setSessionStateKey(string $sessionStateKey): OpenIdConnector {
    $this->sessionStateKey = $sessionStateKey;
    return $this;
  }

  /**
   * @return string
   */
  public function getSessionNonceKey(): string {
    return $this->sessionNonceKey;
  }

  /**
   * @param string $sessionNonceKey
   * @return OpenIdConnector
   */
  public function setSessionNonceKey(string $sessionNonceKey): OpenIdConnector {
    $this->sessionNonceKey = $sessionNonceKey;
    return $this;
  }

  /**
   * Performs an HTTP GET request using a bearer token.
   * Expects a JSON response: any returned value will be fed into json_decode().
   *
   * @param $url string the URL to make the request to
   * @param $bearerToken string the value of the bearer token. It will be inserted in the 'Authorization:' HTTP header.
   * @return mixed The JSON response, decoded.
   * @throws Exception
   */
  public function APIGet($url, $bearerToken) {
    $curl = curl_init($url);
    curl_setopt_array($curl, [
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_HTTPHEADER => [
        'Authorization: Bearer ' . $bearerToken
      ],
    ]);

    $response = curl_exec($curl);
    if ($response === false) {
      throw new CurlException("Cannot retrieve API endpoint. " . curl_error($curl), curl_errno($curl));
    }

    return json_decode($response, true);
  }

  /**
   * Performs the discovery of this provider by visiting the discovery endpoint with Curl.
   * Once parsed, makes sure that $discoveryData contains the following keys:
   *
   * - issuer: URL using the https scheme with no query or fragment component
   *  that the OP asserts as its Issuer Identifier. If Issuer discovery is
   *  supported (see Section 2), this value MUST be identical to the issuer
   *  value returned by WebFinger. This also MUST be identical to the iss
   *  Claim value in ID Tokens issued from this Issuer.
   * - authorization_endpoint: URL of the OP's OAuth 2.0 Authorization
   *  Endpoint [OpenId.Core].
   * - token_endpoint: URL of the OP's OAuth 2.0 Token Endpoint
   *  [OpenId.Core]. This is REQUIRED unless only the Implicit Flow is used.
   * - jwks_uri: URL of the OP's JSON Web Key Set [JWK] document. This
   *  contains the signing key(s) the RP uses to validate signatures from the
   *  OP. The JWK Set MAY also contain the Server's encryption key(s), which
   *  are used by RPs to encrypt requests to the Server. When both signing and
   *  encryption keys are made available, a use (Key Use) parameter value is
   *  REQUIRED for all keys in the referenced JWK Set to indicate each key's
   *  intended usage. Although some algorithms allow the same key to be used
   *  for both signatures and encryption, doing so is NOT RECOMMENDED, as it
   *  is less secure. The JWK x5c parameter MAY be used to provide X.509
   *  representations of keys provided. When used, the bare key values MUST
   *  still be present and MUST match those in the certificate.
   * - response_types_supported: JSON array containing a list of the OAuth
   *  2.0 response_type values that this OP supports. Dynamic OpenId Providers
   *  MUST support the code, id_token, and the token id_token Response Type
   *  values.
   * - subject_types_supported: JSON array containing a list of the Subject
   *  Identifier types that this OP supports. Valid types include pairwise and
   *  public.
   * - id_token_signing_alg_values_supported: JSON array containing a list of
   *  the JWS signing algorithms (alg values) supported by the OP for the ID
   *  Token to encode the Claims in a JWT [JWT]. The algorithm RS256 MUST be
   *  included. The value none MAY be supported, but MUST NOT be used unless
   *  the Response Type used returns no ID Token from the Authorization
   *  Endpoint (such as when using the Authorization Code Flow).
   *
   * Upon successful discovery, it visits the JWKS URI returned by the discovery endpoint.
   *
   * @see https://openid.net/specs/openid-connect-discovery-1_0.html#ProviderMetadata
   *
   * @return mixed
   * @throws CurlException
   * @throws MissingDiscoveryDataException
   */
  public function discover() {

    if ($this->discoveryPerformed) {
      // Avoid making more than one request to the same URL
      return $this->discoveryData;
    }

    $curl_discovery = curl_init($this->discoveryEndpoint);
    curl_setopt($curl_discovery, CURLOPT_RETURNTRANSFER, true);
    $response = curl_exec($curl_discovery);
    if ($response === false) {
      throw new CurlException("Cannot read discovery URI. " . curl_error($curl_discovery), curl_errno($curl_discovery));
    }

    $this->discoveryData = json_decode($response, true);

    // Check mandatory keys
    foreach (static::REQUIRED_DISCOVERY_KEYS as $key) {
      if (!isset($this->discoveryData[$key])) {
        throw new MissingDiscoveryDataException("Mandatory entry missing in discovery data: " . $key);
      }
    }

    // Read json web key storage
    $curl_jwks = curl_init($this->discoveryData['jwks_uri']);
    curl_setopt($curl_jwks, CURLOPT_RETURNTRANSFER, true);
    $jwks_response = curl_exec($curl_jwks);
    if ($jwks_response === false) {
      throw new CurlException("Cannot read jwks URI. " . curl_error($curl_jwks), curl_errno($curl_jwks));
    }

    $this->jwks = json_decode($jwks_response, true);

    $this->discoveryPerformed = true;

    return $this->discoveryData;
  }

  /**
   * Base64-decodes a JWT token.
   *
   * @param string $jwt
   * @return array[object,object,string] of three elements: header, payload and signature
   * @throws Exception
   */
  public function decodeJwtToken(string $jwt) {
    if (empty($this->discoveryData)) {
      $this->discover();
    }

    $segments = explode('.', $jwt);
    if (count($segments) !== 3) {
      throw new WrongSegmentsInTokenException(sprintf("Token should have three segments, %d found", count($segments)));
    }

    return [
      json_decode(JWT::urlsafeB64Decode($segments[0])),
      json_decode(JWT::urlsafeB64Decode($segments[1])),
      JWT::urlsafeB64Decode($segments[2]),
    ];
  }

  /**
   * Decodes and *validates* a JWT token.
   *
   * *Experimental assumptions*: let's call $key the variable holding the key in JWKS.
   * Assumption n.1 : $key["x5c"] is set;
   * Assumption n.2 : $key["x5c"] is an array.
   *
   * Those assumptions are not guaranteed to be met, according to the specs:
   * https://tools.ietf.org/html/draft-ietf-jose-json-web-key-41#section-4.7
   *
   * @todo handle the case when "x5c" is not set, or is not an array, or hasn't got the 0 key.
   *
   * @param string $jwt The JWT token to validate.
   * @return object
   *
   * @throws Exception
   */
  public function validateAndDecodeToken(string $jwt) {

    if (!$this->discoveryData) {
      $this->discover();
    }

    // Retrieve the id of the key used to encrypt this token
    list($header) = $this->decodeJwtToken($jwt);
    $kid = $header->kid;

    // Look it up in the JWKS
    $key = NULL;
    foreach ($this->jwks["keys"] as $keyInStore) {
      if ($kid === $keyInStore["kid"]) {
        // Match
        $key = $keyInStore;
        break;
      }
    }

    if ($key === NULL) {
      throw new KeyNotFoundException(sprintf("Token signed with unknown key (%s)", $kid));
    }

    if (empty($key["x5c"]) || !is_array($key["x5c"]) || !isset($key["x5c"][0])) {
      throw new UnexpectedKeyFormatException(sprintf("Key has unexpected format (%s)", print_r($key, true)));
    }

    $x5c = $key["x5c"][0];

    // Thanks to https://nicksnettravels.builttoroam.com/post/2017/01/24/Verifying-Azure-Active-Directory-JWT-Tokens.aspx
    // We need a .pem format certificate, which means no rows longer than 64 characters
    // Otherwise validation will fail with a message like:
    // "openssl_verify(): supplied key param cannot be coerced into a public key"
    if (strpos($x5c, "\r\n") === false) {
      $x5c = implode("\r\n", str_split($x5c, 64));
    }

    $certificate = "-----BEGIN CERTIFICATE-----\r\n" . $x5c . "\r\n" . "-----END CERTIFICATE-----\r\n";

    $decoded = JWT::decode($jwt, $certificate, $this->discoveryData['id_token_signing_alg_values_supported']);
    return $decoded;
  }

  /**
   * Persists nonce and state in the request's session.
   * The nonce is saved into the variable $this->sessionNonceKey;
   * the state is saved into the variable $this->sessionStateKey.
   *
   * @param Request $request
   * @see destroySessionNonceAndState
   */
  public function saveSessionNonceAndState(Request $request) {
    $session = $request->getSession();
    $session->set($this->sessionNonceKey, $this->nonce);
    $session->set($this->sessionStateKey, $this->state);
  }

  /**
   * Deletes nonce and state from the request's session, that is, sets to false the session variables
   * $this->sessionNonceKey and $this->sessionStateKey.
   *
   * @param Request $request
   * @see saveSessionNonceAndState
   */
  public function destroySessionNonceAndState(Request $request) {
    $session = $request->getSession();
    $session->set($this->sessionNonceKey, false);
    $session->set($this->sessionStateKey, false);
  }

  /**
   *
   * @return mixed
   * @throws Exception
   */
  public function visitTokenEndpoint() {
    $curl = curl_init($this->getTokenEndpoint());
    $params = $this->getTokenEndpointParameters();
    curl_setopt_array($curl, [
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_POST => true,
      CURLOPT_POSTFIELDS => http_build_query($params),
    ]);

    $response = curl_exec($curl);
    if ($response === false) {
      throw new CurlException("Cannot retrieve token endpoint. " . curl_error($curl), curl_errno($curl));
    }

    return json_decode($response, true);
  }

  /**
   * Looks up $this->discovery_data['response_types_supported'] searching for all the members of $wantedTypes.
   * Returns a string with all the matching combinations found, separated by spaces.
   *
   * Examples:
   *
   * $wantedTypes          | $this->discovery_data['response_types_supported'] | Output
   * ----------------------+---------------------------------------------------+------------------------------
   * ['a','b','c']         | ['b a', 'b']                                      | 'b a'
   * ----------------------+---------------------------------------------------+------------------------------
   * ['a','b']             | ['b', 'z a', 'a']                                 | 'b' (first match)
   * ----------------------+---------------------------------------------------+------------------------------
   * ['a','b','c']         | ['z b', 'a', 'c a', 'c b']                        | 'c a' (most populated match
   *                       |                                                   | made of wanted types)
   * ----------------------+---------------------------------------------------+------------------------------
   *
   * @param array $wantedTypes
   * @return string
   * @throws Exception
   */
  public function getSupportedResponseTypeWith(array $wantedTypes) {
    if (empty($this->discoveryData)) {
      $this->discover();
    }

    $supported = $this->discoveryData['response_types_supported'];
    // Response type matching wanted type(s)
    $found = '';
    // Number of matches of $wantedTypes inside $found
    $foundScore = -1;

    foreach ($supported as $sup) {
      if (strpos($sup, ' ') !== false) {
        // list match
        $sup_list = explode(' ', $sup);
        // if every item of $sup_list is in $wantedTypes, then it's a match. Otherwise it isn't
        $match = true; // set to false if one item does not match
        foreach ($sup_list as $item) {
          if (!in_array($item, $wantedTypes)) {
            $match = false;
            break;
          }
        }

        if ($match && $foundScore < count($sup_list)) {
          // After this one, each match of a the same number of values will be ignored
          $found = $sup;
          $foundScore = count($sup_list);
        }

      } else {
        // literal match
        foreach ($wantedTypes as $wanted) {
          // After this one, each literal match of a single value will be ignored
          if ($wanted === $sup && $foundScore < 1) {
            $found = $sup;
            $foundScore = 1;
          }
        }
      }
    }

    return $found;
  }
}
