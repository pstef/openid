<?php

namespace Pstef\OpenId;


/**
 * This exception should be thrown when a key (array) extracted from a JWKS does not have one or more expected members.
 */
class UnexpectedKeyFormatException extends \Exception
{

}