<?php

namespace Pstef\OpenId;


/**
 * Class KeyNotFoundException
 * This exception should be thrown when a JWT token is encountered,
 * which has an unknown "kid" (key id) in the header segment: that is,
 * looking that "kid" up in the JWKS, no key id matches.
 */
class KeyNotFoundException extends \Exception
{

}