<?php

namespace Pstef\OpenId;


/**
 * Class CurlException
 * This exception should be thrown when Curl wasn't able to read an URL.
 */
class CurlException extends \Exception
{
}