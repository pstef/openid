<?php

namespace Pstef\OpenId;

/**
 * Class MissingDiscoveryDataException
 * This exception should be thrown when an OpenId provider does not supply a mandatory value in its discovery data.
 */
class MissingDiscoveryDataException extends \Exception
{

}