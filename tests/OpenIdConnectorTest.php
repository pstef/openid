<?php

namespace Pstef\OpenId;

use PHPUnit\Framework\TestCase;

class OpenIdConnectorTest extends TestCase
{
  /**
   * Given a specific set of supported response types, and the wanted response types,
   * checks that the "most correct" value is returned.
   *
   * @throws \Exception
   */
  public function testGetSupportedResponseTypeWith() {
    $openIdConn = new OpenIdConnector('', '');

    // Literal match cases, 1
    $openIdConn->setDiscoveryData(['response_types_supported' => ['a', 'b', 'c', 'd', 'e', 'f']]);
    $result = $openIdConn->getSupportedResponseTypeWith(['a', 'b', 'c']);
    $this->assertEquals('a', $result);

    // Literal match cases, 2
    $openIdConn->setDiscoveryData(['response_types_supported' => ['f', 'e', 'd', 'c', 'b', 'a']]);
    $result = $openIdConn->getSupportedResponseTypeWith(['a', 'b', 'c']);
    $this->assertEquals('c', $result); // the first $wantedType found in response_types_supported

    // No match cases, 1
    $openIdConn->setDiscoveryData(['response_types_supported' => ['a']]);
    $result = $openIdConn->getSupportedResponseTypeWith(['b', 'c', 'd']);
    $this->assertEquals( '', $result);

    // No match cases, 2
    $openIdConn->setDiscoveryData(['response_types_supported' => ['a', 'b', 'c', 'd']]);
    $result = $openIdConn->getSupportedResponseTypeWith(['e', 'f', 'g']);
    $this->assertEquals( '', $result);

    // More complex cases, 1: match multiple values, return the most populated match, if more than one match then return the first
    $openIdConn->setDiscoveryData(['response_types_supported' => ['z b', 'a', 'c a', 'c b']]);
    $result = $openIdConn->getSupportedResponseTypeWith(['a', 'b', 'h', 'u', 'c']);
    $this->assertEquals( 'c a', $result);

    // More complex cases, 2: match multiple values, return the most populated match
    $openIdConn->setDiscoveryData(['response_types_supported' => ['c a b', 'z b', 'a', 'c a', 'c b']]);
    $result = $openIdConn->getSupportedResponseTypeWith(['a', 'b', 'h', 'u', 'c']);
    $this->assertEquals( 'c a b', $result);

    // More complex cases, 3: match multiple values, return the most populated match
    $openIdConn->setDiscoveryData(['response_types_supported' => ['a', 'z b', 'c a', 'c a b', 'c b']]);
    $result = $openIdConn->getSupportedResponseTypeWith(['a', 'b', 'h', 'u', 'c']);
    $this->assertEquals( 'c a b', $result);
  }
}